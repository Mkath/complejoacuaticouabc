package com.uabc.entidad;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-12-10T16:35:17")
@StaticMetamodel(Curso.class)
public class Curso_ { 

    public static volatile SingularAttribute<Curso, String> instructor;
    public static volatile SingularAttribute<Curso, String> diasImpartido;
    public static volatile SingularAttribute<Curso, String> horasCurso;
    public static volatile SingularAttribute<Curso, String> nombreCurso;
    public static volatile SingularAttribute<Curso, Integer> idcurso;

}