package com.uabc.entidad;

import com.uabc.entidad.Credencial;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-12-10T16:35:17")
@StaticMetamodel(Seguro.class)
public class Seguro_ { 

    public static volatile SingularAttribute<Seguro, String> apellidoPaterno;
    public static volatile SingularAttribute<Seguro, String> numeroSeguro;
    public static volatile SingularAttribute<Seguro, Date> fechaNacimiento;
    public static volatile SingularAttribute<Seguro, String> correo;
    public static volatile SingularAttribute<Seguro, String> egresado;
    public static volatile SingularAttribute<Seguro, String> folio;
    public static volatile ListAttribute<Seguro, Credencial> credencialList;
    public static volatile SingularAttribute<Seguro, String> nombre;
    public static volatile SingularAttribute<Seguro, Integer> idseguro;
    public static volatile SingularAttribute<Seguro, String> apellidoMaterno;

}