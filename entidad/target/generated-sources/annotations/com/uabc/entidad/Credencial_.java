package com.uabc.entidad;

import com.uabc.entidad.Seguro;
import com.uabc.entidad.Visitas;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-12-10T16:35:17")
@StaticMetamodel(Credencial.class)
public class Credencial_ { 

    public static volatile SingularAttribute<Credencial, String> numeroEmergencia;
    public static volatile SingularAttribute<Credencial, String> tipoCredencial;
    public static volatile SingularAttribute<Credencial, String> apellidoPaterno;
    public static volatile SingularAttribute<Credencial, Date> fehcaExpiracion;
    public static volatile SingularAttribute<Credencial, String> nombreEmergencia;
    public static volatile SingularAttribute<Credencial, String> correo;
    public static volatile SingularAttribute<Credencial, Integer> hrsDisponibles;
    public static volatile SingularAttribute<Credencial, String> nombreUsuario;
    public static volatile SingularAttribute<Credencial, Integer> idcredencial;
    public static volatile SingularAttribute<Credencial, String> apellidoMaterno;
    public static volatile ListAttribute<Credencial, Visitas> visitasList;
    public static volatile SingularAttribute<Credencial, Seguro> idseguro;

}