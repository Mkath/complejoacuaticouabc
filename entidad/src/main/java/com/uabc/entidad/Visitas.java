/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uabc.entidad;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author user
 */
@Entity
@Table(name = "visitas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Visitas.findAll", query = "SELECT v FROM Visitas v")
    , @NamedQuery(name = "Visitas.findByIdvisitas", query = "SELECT v FROM Visitas v WHERE v.idvisitas = :idvisitas")
    , @NamedQuery(name = "Visitas.findByHoras", query = "SELECT v FROM Visitas v WHERE v.horas = :horas")
    , @NamedQuery(name = "Visitas.findByTurno", query = "SELECT v FROM Visitas v WHERE v.turno = :turno")
    , @NamedQuery(name = "Visitas.findByTipoPago", query = "SELECT v FROM Visitas v WHERE v.tipoPago = :tipoPago")})
public class Visitas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idvisitas")
    private Integer idvisitas;
    @Basic(optional = false)
    @Column(name = "horas")
    private int horas;
    @Basic(optional = false)
    @Column(name = "turno")
    private String turno;
    @Basic(optional = false)
    @Column(name = "tipo_pago")
    private String tipoPago;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idvisita")
    private List<CorteTurno> corteTurnoList;
    @JoinColumn(name = "idcredencial", referencedColumnName = "idcredencial")
    @ManyToOne(optional = false)
    private Credencial idcredencial;

    public Visitas() {
    }

    public Visitas(Integer idvisitas) {
        this.idvisitas = idvisitas;
    }

    public Visitas(Integer idvisitas, int horas, String turno, String tipoPago) {
        this.idvisitas = idvisitas;
        this.horas = horas;
        this.turno = turno;
        this.tipoPago = tipoPago;
    }

    public Integer getIdvisitas() {
        return idvisitas;
    }

    public void setIdvisitas(Integer idvisitas) {
        this.idvisitas = idvisitas;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    @XmlTransient
    public List<CorteTurno> getCorteTurnoList() {
        return corteTurnoList;
    }

    public void setCorteTurnoList(List<CorteTurno> corteTurnoList) {
        this.corteTurnoList = corteTurnoList;
    }

    public Credencial getIdcredencial() {
        return idcredencial;
    }

    public void setIdcredencial(Credencial idcredencial) {
        this.idcredencial = idcredencial;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idvisitas != null ? idvisitas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Visitas)) {
            return false;
        }
        Visitas other = (Visitas) object;
        if ((this.idvisitas == null && other.idvisitas != null) || (this.idvisitas != null && !this.idvisitas.equals(other.idvisitas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uabc.entidad.Visitas[ idvisitas=" + idvisitas + " ]";
    }
    
}
