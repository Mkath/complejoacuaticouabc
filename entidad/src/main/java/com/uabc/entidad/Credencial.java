/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uabc.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author user
 */
@Entity
@Table(name = "credencial")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Credencial.findAll", query = "SELECT c FROM Credencial c")
    , @NamedQuery(name = "Credencial.findByIdcredencial", query = "SELECT c FROM Credencial c WHERE c.idcredencial = :idcredencial")
    , @NamedQuery(name = "Credencial.findByHrsDisponibles", query = "SELECT c FROM Credencial c WHERE c.hrsDisponibles = :hrsDisponibles")
    , @NamedQuery(name = "Credencial.findByFehcaExpiracion", query = "SELECT c FROM Credencial c WHERE c.fehcaExpiracion = :fehcaExpiracion")
    , @NamedQuery(name = "Credencial.findByNombreUsuario", query = "SELECT c FROM Credencial c WHERE c.nombreUsuario = :nombreUsuario")
    , @NamedQuery(name = "Credencial.findByApellidoPaterno", query = "SELECT c FROM Credencial c WHERE c.apellidoPaterno = :apellidoPaterno")
    , @NamedQuery(name = "Credencial.findByApellidoMaterno", query = "SELECT c FROM Credencial c WHERE c.apellidoMaterno = :apellidoMaterno")
    , @NamedQuery(name = "Credencial.findByCorreo", query = "SELECT c FROM Credencial c WHERE c.correo = :correo")
    , @NamedQuery(name = "Credencial.findByNumeroEmergencia", query = "SELECT c FROM Credencial c WHERE c.numeroEmergencia = :numeroEmergencia")
    , @NamedQuery(name = "Credencial.findByNombreEmergencia", query = "SELECT c FROM Credencial c WHERE c.nombreEmergencia = :nombreEmergencia")
    , @NamedQuery(name = "Credencial.findByTipoCredencial", query = "SELECT c FROM Credencial c WHERE c.tipoCredencial = :tipoCredencial")})
public class Credencial implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcredencial")
    private Integer idcredencial;
    @Basic(optional = false)
    @Column(name = "hrs_disponibles")
    private int hrsDisponibles;
    @Basic(optional = false)
    @Column(name = "fehca_expiracion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fehcaExpiracion;
    @Basic(optional = false)
    @Column(name = "nombre_usuario")
    private String nombreUsuario;
    @Basic(optional = false)
    @Column(name = "apellido_paterno")
    private String apellidoPaterno;
    @Basic(optional = false)
    @Column(name = "apellido_materno")
    private String apellidoMaterno;
    @Basic(optional = false)
    @Column(name = "correo")
    private String correo;
    @Basic(optional = false)
    @Column(name = "numero_emergencia")
    private String numeroEmergencia;
    @Basic(optional = false)
    @Column(name = "nombre_emergencia")
    private String nombreEmergencia;
    @Basic(optional = false)
    @Column(name = "tipo_credencial")
    private String tipoCredencial;
    @JoinColumn(name = "idseguro", referencedColumnName = "idseguro")
    @ManyToOne(optional = false)
    private Seguro idseguro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcredencial")
    private List<Visitas> visitasList;

    public Credencial() {
    }

    public Credencial(Integer idcredencial) {
        this.idcredencial = idcredencial;
    }

    public Credencial(Integer idcredencial, int hrsDisponibles, Date fehcaExpiracion, String nombreUsuario, String apellidoPaterno, String apellidoMaterno, String correo, String numeroEmergencia, String nombreEmergencia, String tipoCredencial) {
        this.idcredencial = idcredencial;
        this.hrsDisponibles = hrsDisponibles;
        this.fehcaExpiracion = fehcaExpiracion;
        this.nombreUsuario = nombreUsuario;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.correo = correo;
        this.numeroEmergencia = numeroEmergencia;
        this.nombreEmergencia = nombreEmergencia;
        this.tipoCredencial = tipoCredencial;
    }

    public Integer getIdcredencial() {
        return idcredencial;
    }

    public void setIdcredencial(Integer idcredencial) {
        this.idcredencial = idcredencial;
    }

    public int getHrsDisponibles() {
        return hrsDisponibles;
    }

    public void setHrsDisponibles(int hrsDisponibles) {
        this.hrsDisponibles = hrsDisponibles;
    }

    public Date getFehcaExpiracion() {
        return fehcaExpiracion;
    }

    public void setFehcaExpiracion(Date fehcaExpiracion) {
        this.fehcaExpiracion = fehcaExpiracion;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getNumeroEmergencia() {
        return numeroEmergencia;
    }

    public void setNumeroEmergencia(String numeroEmergencia) {
        this.numeroEmergencia = numeroEmergencia;
    }

    public String getNombreEmergencia() {
        return nombreEmergencia;
    }

    public void setNombreEmergencia(String nombreEmergencia) {
        this.nombreEmergencia = nombreEmergencia;
    }

    public String getTipoCredencial() {
        return tipoCredencial;
    }

    public void setTipoCredencial(String tipoCredencial) {
        this.tipoCredencial = tipoCredencial;
    }

    public Seguro getIdseguro() {
        return idseguro;
    }

    public void setIdseguro(Seguro idseguro) {
        this.idseguro = idseguro;
    }

    @XmlTransient
    public List<Visitas> getVisitasList() {
        return visitasList;
    }

    public void setVisitasList(List<Visitas> visitasList) {
        this.visitasList = visitasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcredencial != null ? idcredencial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Credencial)) {
            return false;
        }
        Credencial other = (Credencial) object;
        if ((this.idcredencial == null && other.idcredencial != null) || (this.idcredencial != null && !this.idcredencial.equals(other.idcredencial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.uabc.entidad.Credencial[ idcredencial=" + idcredencial + " ]";
    }
    
}
