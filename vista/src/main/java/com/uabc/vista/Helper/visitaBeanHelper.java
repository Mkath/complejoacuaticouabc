 
package com.uabc.vista.Helper;

import com.uabc.negocio.integracion.serviceFacadeLocator;
import complejo.entidad.Visitas;
import java.io.Serializable;
import java.util.List;

/** 
 * @author Axel Valenzuela - Complejo Acuatico 2018
 */
public class visitaBeanHelper  implements Serializable{
    /**
     * Metodo para guardar una visita 
     * @param visitas
     */
    public void agregarNuevaVisitas(Visitas visitas){ 
        serviceFacadeLocator.getInstanceVisitasFacade().saveVisitas(visitas);
    }
    /**
     * Metodo para buscar una Visita por ID
     * @param id 
     * @return  
     */
    public Visitas consultarVisitasByID(int id){ 
     return serviceFacadeLocator.getInstanceVisitasFacade().findByID(id);
    }
    
    /**
     * Metodo para buscar todos las Visitas 
     * @param id 
     * @return  
     */
    public List<Visitas> consultarTodosLasVisitas(int id){ 
     return serviceFacadeLocator.getInstanceVisitasFacade().findAll();
    } 
    /**
     * Metodo para modificar una visita 
     * @param visitas
     */
    public void updateVisitas(Visitas visitas){
        serviceFacadeLocator.getInstanceVisitasFacade().updateVisitas(visitas);
    }    
}
