 
package com.uabc.negocio.facade;
import com.uabc.negocio.delegate.delegateVisitas;
import java.util.List;
import complejo.entidad.Visitas;  
/**
 * @author Axel Valenzuela - Complejo Acuático 2018
 */
public class visitasFacade {

    delegateVisitas delegateVisitas = new delegateVisitas();

    /**
     * Metodo para guardar una visita
     *
     * @param visitas
     */
    public void saveVisitas(Visitas visitas) {
        delegateVisitas.saveVisitas(visitas);
    }

    /**
     *
     * @return Lista de visitas encontradas
     */
    public List<Visitas> findAll() {
        return delegateVisitas.findAllVisitas();
    }

    /**
     *
     * @param id ID de la visita a buscar
     * @return Administrador encontrada
     */
    public Visitas findByID(int id) {
        return delegateVisitas.findByID(id);
    }

     /**
     *
     * @param visitas para realizar actualización de la visista.
     */
    public void updateVisitas(Visitas visitas) {
        delegateVisitas.updateVisitas(visitas);
    }
 
 

}
