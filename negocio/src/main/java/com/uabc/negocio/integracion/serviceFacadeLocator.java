package com.uabc.negocio.integracion;

import com.uabc.negocio.facade.administradorFacade;
import com.uabc.negocio.facade.credencialFacade;
import com.uabc.negocio.facade.cursoFacade;
import com.uabc.negocio.facade.seguroFacade;
import com.uabc.negocio.facade.visitasFacade;

/**
 *
 * @author Carlos Arellano - Complejo Acuatico 2018
 */
public class serviceFacadeLocator {

    private static administradorFacade administradorFacade;
    private static credencialFacade credencialFacade;
    private static cursoFacade cursoFacade;
    private static seguroFacade seguroFacade; 
    private static visitasFacade visitasFacade;
    /**
     *
     * @return administradorFacade
     */ 
    public static administradorFacade getInstanceAdministradorFacade() {
        if (administradorFacade == null) {
            administradorFacade = new administradorFacade();
            return administradorFacade;
        } else {
            return administradorFacade;
        }
    }

    /**
     *
     * @return credencialFacade
     */
    public static credencialFacade getInstanceCredencialFacade() {
        if (credencialFacade == null) {
            credencialFacade = new credencialFacade();
            return credencialFacade;
        } else {
            return credencialFacade;
        }
    }

    /**
     *
     * @return cursoFacade
     */
    public static cursoFacade getInstanceCursosFacade() {
        if (cursoFacade == null) {
            cursoFacade = new cursoFacade();
            return cursoFacade;
        } else {
            return cursoFacade;
        }
    }

    /**
     *
     * @return seguroFacade
     */
    public static seguroFacade getInstanceSeguroFacade() {
        if (seguroFacade == null) {
            seguroFacade = new seguroFacade();
            return seguroFacade;
        } else {
            return seguroFacade;
        }
    }
    
    /**
     *
     * @return seguroFacade
     */
    public static visitasFacade getInstanceVisitasFacade() {
        if (visitasFacade == null) {
            visitasFacade = new visitasFacade();
            return visitasFacade;
        } else {
            return visitasFacade;
        }
    }
 
}
