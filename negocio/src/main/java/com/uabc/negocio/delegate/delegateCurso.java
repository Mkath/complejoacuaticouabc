package com.uabc.negocio.delegate;

import complejo.entidad.Curso;
import com.uabc.complejo.integracion.serviceLocator;
import java.util.List;

/**
 *
 * @author Axel Valenzuela - Complejo Acuático 2018
 */
public class delegateCurso {

    /**
     *
     * @param cursos que se va a guardar
     */
    public void saveCursos(Curso cursos) {
        Curso result = null;
        if (cursos.getIdcurso() != 0) {
            result = serviceLocator.getInstanceCursoDAO().findByCursosId(cursos.getIdcurso());
        }
        if (result != null) {
            serviceLocator.getInstanceCursoDAO().updateCursos(cursos);
        } else {
            serviceLocator.getInstanceCursoDAO().addCursos(cursos);
        }
    }

    /**
     * @param id de la Cursos que se va a buscar
     * @return Cursos encontrada
     */
    public Curso findByID(int id) {
        return serviceLocator.getInstanceCursoDAO().findByCursosId(id);
    }

    /**
     * @return Todos los Cursos encontrados
     */
    public List<Curso> findCursos() {
        return serviceLocator.getInstanceCursoDAO().findAllCursos();
    }

    /**
     * @param cursos para actualizar
     */
    public void updateCursos(Curso cursos) {
        serviceLocator.getInstanceCursoDAO().updateCursos(cursos);
    }

    /**
     * @param id para eliminar
     */
    public void eliminarCursos(int id) {
        serviceLocator.getInstanceCursoDAO().deleteCursos(id);
    }

}
