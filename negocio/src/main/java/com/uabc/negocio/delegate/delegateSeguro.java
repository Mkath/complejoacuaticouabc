package com.uabc.negocio.delegate;

import complejo.entidad.Seguro;
import com.uabc.complejo.integracion.serviceLocator;
import java.util.List;

/**
 *
 * @author Axel Valenzuela - Complejo Acuático 2018
 */
public class delegateSeguro {

    /**
     *
     * @param seguro que se va a guardar
     */
    public void saveSeguro(Seguro seguro) {
        Seguro result = null;
        if (seguro.getIdseguro() != 0) {
            result = serviceLocator.getInstanceSeguroDAO().findBySeguroId(seguro.getIdseguro());
        }
        if (result != null) {
            serviceLocator.getInstanceSeguroDAO().updateSeguro(seguro);
        } else {
            serviceLocator.getInstanceSeguroDAO().addSeguro(seguro);
        }
    }

    /**
     * @param id de la Seguro que se va a buscar
     * @return Seguro encontrada
     */
    public Seguro findByID(int id) {
        return serviceLocator.getInstanceSeguroDAO().findBySeguroId(id);
    }

    /**
     * @return Todos los Seguro encontrados
     */
    public List<Seguro> findSeguro() {
        return serviceLocator.getInstanceSeguroDAO().findAllSeguro();
    }

    /**
     * @param seguro para actualizar
     */
    public void updateSeguro(Seguro seguro) {
        serviceLocator.getInstanceSeguroDAO().updateSeguro(seguro);
    }

    /**
     * @param id para eliminar
     */
    public void eliminarSeguro(int id) {
        serviceLocator.getInstanceSeguroDAO().deleteSeguro(id);
    }

}
