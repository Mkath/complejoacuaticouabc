package com.uabc.negocio.delegate;

import complejo.entidad.Visitas;
import com.uabc.complejo.integracion.serviceLocator;
import java.util.List;

/** 
 * @author Axel Valenzuela - Complejo Acuático 2018
 */
public class delegateVisitas {

    /** 
     * @param visitas
     */
    public void saveVisitas(Visitas visitas) {
        Visitas result = null;
        if (visitas.getIdvisitas() != 0) {
            result = serviceLocator.getInstanceVisitasDAO().findByVisitasId(visitas.getIdvisitas());
        }
        if (result != null) {
            serviceLocator.getInstanceVisitasDAO().updateVisitas(visitas);
        } else {
            serviceLocator.getInstanceVisitasDAO().addVisitas(visitas);
        }
    }

    /**
     * @param id de la Administrador que se va a buscar
     * @return Administrador encontrada
     */
    public Visitas findByID(int id) {
        return serviceLocator.getInstanceVisitasDAO().findByVisitasId(id);
    }

    /**
     * @return Todos los Visitas encontrados
     */
    public List<Visitas> findAllVisitas() {
        return serviceLocator.getInstanceVisitasDAO().findAllVisitas();
    }

    /**
     * @param visitas
     */
    public void updateVisitas(Visitas visitas) {
        serviceLocator.getInstanceVisitasDAO().updateVisitas(visitas);
    }
 
}
