package com.uabc.negocio.delegate;

import complejo.entidad.Crendencial;
import com.uabc.complejo.integracion.serviceLocator;
import java.util.List;

/**
 *
 * @author Axel Valenzuela - Complejo Acuático 2018
 */
public class delegateCredencial {

    /**
     *
     * @param credencial que se va a guardar
     */
    public void saveCredencial(Crendencial credencial) {
        Crendencial result = null;
        if (credencial.getIdcrendencial() != 0) {
            result = serviceLocator.getInstanceCredencialDAO().findByCredencialId(credencial.getIdcrendencial());
        }
        if (result != null) {
            serviceLocator.getInstanceCredencialDAO().updateCredencial(credencial);
        } else {
            serviceLocator.getInstanceCredencialDAO().addCredencial(credencial);
        }
    }

    /**
     * @param id de la Credencial que se va a buscar
     * @return Credencial encontrada
     */
    public Crendencial findByID(int id) {
        return serviceLocator.getInstanceCredencialDAO().findByCredencialId(id);
    }

    /**
     * @return Todos los Credencial encontrados
     */
    public List<Crendencial> findCredencial() {
        return serviceLocator.getInstanceCredencialDAO().findAllCredencial();
    }

    /**
     * @param credencial para actualizar
     */
    public void updateCredencial(Crendencial credencial) {
        serviceLocator.getInstanceCredencialDAO().updateCredencial(credencial);
    }

    /**
     * @param id para eliminar
     */
    public void eliminarCredencial(int id) {
        serviceLocator.getInstanceCredencialDAO().deleteCredencial(id);
    }

}
