package com.uabc.negocio.delegate;

import complejo.entidad.Administrador;  
import com.uabc.complejo.integracion.serviceLocator;

import java.util.List;

/**
 *
 * @author Axel Valenzuela - Complejo Acuático 2018
 */
public class delegateAdministrador {

    /**
     *
     * @param Administrador que se va a guardar
     */
    public void saveAdministrador(Administrador administrador) {
        Administrador result = null;
        if (administrador.getIdadministrador() != 0) {
            result = serviceLocator.getInstanceAdminDAO().findByAdminId(administrador.getIdadministrador());
        }
        if (result != null) {
            serviceLocator.getInstanceAdminDAO().updateAdmin(administrador);
        } else {
            serviceLocator.getInstanceAdminDAO().addAdmin(administrador);
        }
    }

    /**
     * @param id de la Administrador que se va a buscar
     * @return Administrador encontrada
     */
    public Administrador findByID(int id) {
        return serviceLocator.getInstanceAdminDAO().findByAdminId(id);
    }

    /**
     * @return Todos los Administrador encontrados
     */
    public List<Administrador> findAdministrador() {
        return serviceLocator.getInstanceAdminDAO().findAllAdmin();
    }

    /**
     * @param administrador para actualizar
     */
    public void updateAdministrador(Administrador administrador) {
        serviceLocator.getInstanceAdminDAO().updateAdmin(administrador);
    }

    /**
     * @param administrador para eliminar
     */
    public void eliminarAdministrador(int id) {
        serviceLocator.getInstanceAdminDAO().deleteAdmin(id);
    }

}
