package com.uabc.dao;
import com.uabc.entidad.Credencial; 
import com.uabc.persistencia.AbstractDAO;
import java.util.List;
/**
 *
 * @author Carlos Arellano - Complejo Acuatico 2018
 */
public class credencialDAO extends AbstractDAO<Integer, Credencial> {

    public void addCredencial(Credencial credencial) {
        this.save(credencial);
    }

    public void updateCredencial(Credencial credencial) {
        this.update(credencial);
    }

    public void deleteCredencial(int id) {
        Credencial u = this.find(id);
        if (u != null) {
            this.delete(u);
        }
    }

    public List<Credencial> findAllCredencial() {
        return this.findAll();
    }

    public Credencial findByCredencialId(int id) {
        return this.find(id);
    }
 
}

    
