package com.uabc.dao;
import com.uabc.entidad.Visitas;
import com.uabc.persistencia.AbstractDAO;
import java.util.List;

public class visitasDAO extends AbstractDAO<Integer, Visitas> {

    public void addVisitas(Visitas visitas) {
        this.save(visitas);
    }

    public void updateVisitas(Visitas visitas) {
        this.update(visitas);
    }

    public void deleteVisitas(int id) {
        Visitas u = this.find(id);
        if (u != null) {
            this.delete(u);
        }
    }

    public List<Visitas> findAllVisitas() {
        return this.findAll();
    }

    public Visitas findByVisitasId(int id) {
        return this.find(id);
    }
}
