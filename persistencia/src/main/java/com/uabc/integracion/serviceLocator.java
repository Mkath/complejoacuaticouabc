package com.uabc.integracion;

import com.uabc.dao.adminDAO;
import com.uabc.dao.cursoDAO;
import com.uabc.dao.credencialDAO;
import com.uabc.dao.seguroDAO;
import com.uabc.dao.visitasDAO; 

/**
 *
 * @author Carlos Arellano
 */
public class serviceLocator {

    private static adminDAO adminDAO;
    private static credencialDAO credencialDAO;
    private static cursoDAO cursoDAO;
    private static seguroDAO seguroDAO;
    private static visitasDAO visitasDAO;

    public static adminDAO getInstanceAdminDAO() {
        if (adminDAO == null) {
            adminDAO = new adminDAO();
            return adminDAO;
        } else {
            return adminDAO;
        }
    }

    public static seguroDAO getInstanceSeguroDAO() {
        if (seguroDAO == null) {
            seguroDAO = new seguroDAO();
            return seguroDAO;
        } else {
            return seguroDAO;
        }
    }

    public static cursoDAO getInstanceCursoDAO() {
        if (cursoDAO == null) {
            cursoDAO = new cursoDAO();
            return cursoDAO;
        } else {
            return cursoDAO;
        }
    }

    public static credencialDAO getInstanceCredencialDAO() {
        if (credencialDAO == null) {
            credencialDAO = new credencialDAO();
            return credencialDAO;
        } else {
            return credencialDAO;
        }
    }

    public static visitasDAO getInstanceVisitasDAO() {
        if (visitasDAO == null) {
            visitasDAO = new visitasDAO();
            return visitasDAO;
        } else {
            return visitasDAO;
        }
    }
}
